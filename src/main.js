// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
import App from './App'
require('../node_modules/milligram/dist/milligram.min.css')

Vue.config.productionTip = false

Vue.use(VueAnalytics, {
    id: process.env.GA_TRACKER,
    checkDuplicatedScript: true,
    autoTracking: {
        exception: true
    },
    beforeFirstHit () {
        Vue.$ga.set('anonymizeIp', true)
    },
    debug: {
        enabled: !(process.env.NODE_ENV === 'production'),
        trace: true,
        sendHitTask: true
    }
})

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
})
