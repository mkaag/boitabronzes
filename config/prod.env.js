'use strict'
const GitRevisionPlugin = require('git-revision-webpack-plugin')
const gitRevisionPlugin = new GitRevisionPlugin()

module.exports = {
    NODE_ENV: '"production"',
    GA_TRACKER: "'UA-21705039-15'",
    GIT_VERSION: JSON.stringify(gitRevisionPlugin.version()),
    GIT_HASH: JSON.stringify(gitRevisionPlugin.commithash()),
    GIT_BRANCH: JSON.stringify(gitRevisionPlugin.branch())
}
