GIT_USER_NAME := $(shell git config --get --global user.name)
GIT_USER_EMAIL := $(shell git config --get --global user.email)

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: update

install: git setup

update: ## Update the local Git repo as well as brew installed packages
	@echo 'Updating $(shell pwd)'
	@git pull origin master
	@echo 'Updating Homebrew'
	@type brew >/dev/null 2>&1 && brew update && brew upgrade --all || true

git: ## Configure username and email for Git
	@echo 'Configuring git'
	@ssh-add ~/.ssh/gitlab_rsa
	@git config --global color.ui true
	@read -r -p "Name ($(GIT_USER_NAME)): " NAME; \
	 if [ ! -z "$$NAME" ]; then \
	   git config --global user.name "$$NAME"; \
	 fi
	@read -r -p "Email ($(GIT_USER_EMAIL)): " EMAIL; \
	 if [ ! -z "$$EMAIL" ]; then \
	   git config --global user.email "$$EMAIL"; \
	 fi

setup: ## Install AWS-CLI, Hugo, NVM, NodeJS and Terraform
	@echo 'Installing Homebrew packages'
	brew install awscli nvm terraform
	@echo 'Configuring NodeJS using NVM'
	source $$(brew --prefix nvm)/nvm.sh && nvm install stable
	source $$(brew --prefix nvm)/nvm.sh && nvm alias default stable
	@echo 'Installing NodeJS dependencies'
	source $$(brew --prefix nvm)/nvm.sh && npm install
	@echo 'Configuring AWS-CLI'
	@type aws >/dev/null 2>&1 && aws configure  || true

dev: ## Run in development mode
	@echo 'Running in development mode'
	@rm -fr ./static/
	source $$(brew --prefix nvm)/nvm.sh && npm run dev

prod: ## Run in production mode
	@echo 'Run in production mode'
	source $$(brew --prefix nvm)/nvm.sh && npm run build

clean: ## Remove untracked files
	@echo 'Cleaning'
	rm -fr ./node_modules/
