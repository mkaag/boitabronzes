# boitabronzes

> La Boite à Bronzés

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
make dev

# build for production with minification
make prod

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
